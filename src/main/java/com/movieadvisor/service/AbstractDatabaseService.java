package com.movieadvisor.service;

import java.io.Serializable;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.repository.CrudRepository;

public abstract class AbstractDatabaseService<T, ID extends Serializable> implements DatabaseService<T,ID> {

	private static final Logger LOGGER = LoggerFactory.getLogger(AbstractDatabaseService.class);
	
	@Override
	public T persist(T t) {
		LOGGER.info("persisting instance ...");
		return getRepository().save(t);
	}
	
	@Override
	public T update(T t) {
		LOGGER.info("updating instance ...");
		return getRepository().save(t);
	}
	
	@Override
	public T findById(ID id) {
		LOGGER.info("looking instance by id ...");
		return getRepository().findOne(id);
	}
	
	@Override
	public void delete(ID id) {
		LOGGER.info("deleting instance by id ...");		
		getRepository().delete(id);
	}
	
	protected abstract CrudRepository<T, ID> getRepository();
	
	protected abstract T buildDomain();
	
}
