package com.movieadvisor.service;

import com.movieadvisor.repository.domain.Actor;

public interface ActorService extends DatabaseService<Actor, Long>, SuggestionService {

}
