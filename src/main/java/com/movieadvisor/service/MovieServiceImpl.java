package com.movieadvisor.service;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import javax.persistence.EntityManager;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.RandomStringUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.exception.ExceptionUtils;
import org.apache.commons.lang.math.RandomUtils;
import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.queryParser.MultiFieldQueryParser;
import org.apache.lucene.queryParser.ParseException;
import org.apache.lucene.search.Query;
import org.apache.lucene.util.Version;
import org.hibernate.search.jpa.FullTextEntityManager;
import org.hibernate.search.jpa.FullTextQuery;
import org.hibernate.search.query.dsl.QueryBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.movieadvisor.bo.Suggestion;
import com.movieadvisor.repository.dao.MovieDAO;
import com.movieadvisor.repository.domain.Movie;

@Service
@Transactional
public class MovieServiceImpl extends AbstractDatabaseService<Movie,Long> implements MovieService {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(MovieServiceImpl.class);

	private static final String TITLE_EDGE_NGRAM_INDEX = "edgeNGramTitle";
	private static final String TITLE_NGRAM_INDEX = "nGramTitle";	
	
	@Autowired
	private MovieDAO movieDAO;
	
	@Autowired
	private EntityManager entityManager;
	
	@Autowired
	private FullTextEntityManager fullTextEntityManager;
	
	@Autowired
	private GenreService genreService;
	
	@Autowired
	private ActorService actorService; 

	@Override
	protected CrudRepository<Movie, Long> getRepository() {
		return movieDAO;
	}

	@Override
	protected Movie buildDomain() {
		Movie movie = new Movie();
		
		String title = RandomStringUtils.randomAlphabetic(25);		
		int random = RandomUtils.nextInt(26) + 1;
		
		if (random % 3 == 0) {			
			StringUtils.replace(title, Character.toString(title.charAt(random)), " ");
		}
		
		movie.setTitle(title);
		movie.setDirectorName(RandomStringUtils.randomAlphabetic(5) + " " + RandomStringUtils.randomAlphabetic(5));
		movie.setRating(RandomUtils.nextInt(7));
		
		return movie;
	}

	@Override
	public Long findMaxId() {
		return movieDAO.getMaxId();
	}

	@Override
	public Long findMinId() {
		return movieDAO.getMinId();
	}

	@Override
	public void initMovieConnections(Long minMovieId, Long maxMovieId, int size) {
		throw new UnsupportedOperationException();
	}

	@Override
	public void initRandomData(int size) {

		InputStream in = this.getClass().getClassLoader().getResourceAsStream("films.txt");		
		List<String> movies;
		try {
			movies = IOUtils.readLines(in);
		} catch (IOException e) {
			throw new IllegalStateException(e);
		}
		
		Movie m = null;
		String movie = null;
		int i = 0;
		
		while (i < movies.size() && i < size) {
			movie = movies.get(i);
			try {
				if (movie.length() > 100)
					continue;

				m = buildDomain();
				
				m.setTitle(movie);
				
				movieDAO.saveAndFlush(m);
				
				i++;
			} catch (Throwable t) {
				LOGGER.error(ExceptionUtils.getMessage(t));
			}
		}

		while ( i < size ) {
			try {
				movieDAO.saveAndFlush(buildDomain());
				
				i++;
			} catch (Throwable t) {
				LOGGER.error(ExceptionUtils.getMessage(t));
			}
		}		
	}
	
	@Override
	public List<Suggestion> suggest(String term) {
		LOGGER.info("searching suggestions : " + term);
		
		if (!term.endsWith("*")) {
			term += "*";
		}
		
		List<Suggestion> movies = suggestMovies(term);		
		Set<Suggestion> result = new TreeSet<>(movies);
		
		result.addAll(genreService.suggest(term));		
		result.addAll(actorService.suggest(term));		
		
		LOGGER.info("searched suggestions");
		
		return new ArrayList<>(result);
	}

	@Override
	public List<com.movieadvisor.bo.Movie> search(final String term) {
		LOGGER.info("searching movie : " + term);
		
		String[] fields = new String[] {"title", "actors.firstName", "actors.lastName", "genres.name"};
		MultiFieldQueryParser parser = new MultiFieldQueryParser(Version.LUCENE_36, fields, new StandardAnalyzer(Version.LUCENE_36));
		
		org.apache.lucene.search.Query query;

		try {
			query = parser.parse(term);
		} catch (ParseException e) {
			LOGGER.error(ExceptionUtils.getStackTrace(e));			
			return Collections.emptyList();
		}
				
		FullTextQuery fullTextQuery = fullTextEntityManager.createFullTextQuery(query, Movie.class);		
		
		fullTextQuery.setProjection("id", "directorName", "rating", "title");
		fullTextQuery.setMaxResults(20);
		
		@SuppressWarnings("unchecked")
		List<Object[]> list = fullTextQuery.getResultList();
		List<com.movieadvisor.bo.Movie> webMovies = new ArrayList<>(list.size());
		
		com.movieadvisor.bo.Movie webMovie;
    	
		for (Object[] dm : list) {
    		webMovie = new com.movieadvisor.bo.Movie();
    		
    		webMovie.setId((Long) dm[0]);
    		webMovie.setDirectorName((String) dm[1]);
    		webMovie.setRating((Integer) dm[2]);
    		webMovie.setTitle((String) dm[3]);
    		
    		System.out.println(String.format("%d %s %d %s", dm[0],dm[1],dm[2],dm[3]));

    		webMovies.add(webMovie);
    	}
    	
    	Collections.sort(webMovies, new SearchTermMovieComparator(term));

    	LOGGER.info("searched movie");
    	
    	return webMovies;
	}

	private List<Suggestion> suggestMovies(String term) {
		QueryBuilder titleQB = fullTextEntityManager.getSearchFactory()
				.buildQueryBuilder().forEntity(Movie.class).get();
		
		Query query = titleQB.phrase().withSlop(2)
				.onField(TITLE_NGRAM_INDEX)
				.andField(TITLE_EDGE_NGRAM_INDEX).boostedTo(5)
				.sentence(term.toLowerCase())
				.createQuery();
		
		FullTextQuery fullTextQuery = fullTextEntityManager.createFullTextQuery(query, Movie.class);
		
		fullTextQuery.setProjection("title",FullTextQuery.SCORE);
		fullTextQuery.setMaxResults(20);
		
		@SuppressWarnings("unchecked")
		List<Object[]> list = fullTextQuery.getResultList();
		
		List<Suggestion> suggestions = new ArrayList<>(list.size());		
		for(Object[] obj : list) {
			Suggestion suggestion = new Suggestion();
			
			suggestion.setText(String.valueOf(obj[0]));
			suggestion.setScore((Float) obj[1]);
			
			suggestions.add(suggestion);
		}
		
		return suggestions;
	}
	
	private class SearchTermMovieComparator implements Comparator<com.movieadvisor.bo.Movie> {
		
		private String searchTerm;
		
		public SearchTermMovieComparator(String searchTerm) {
			this.searchTerm = searchTerm;
		}
		
		@Override
		public int compare(com.movieadvisor.bo.Movie m1, com.movieadvisor.bo.Movie m2) {
			if (m1.getTitle().equalsIgnoreCase(searchTerm)) {
				return -1;
			}
			
			if (m2.getTitle().equalsIgnoreCase(searchTerm)) {
				return 1;
			}
			
			return -1 * m1.getRating().compareTo(m2.getRating());
		}
	}
}
