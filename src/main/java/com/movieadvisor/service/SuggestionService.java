package com.movieadvisor.service;

import java.util.List;

import com.movieadvisor.bo.Suggestion;

public interface SuggestionService {
	List<Suggestion> suggest(String term);
}
