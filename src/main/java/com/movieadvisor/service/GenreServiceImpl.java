package com.movieadvisor.service;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.RandomStringUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.exception.ExceptionUtils;
import org.apache.commons.lang.math.RandomUtils;
import org.apache.lucene.search.Query;
import org.hibernate.search.jpa.FullTextEntityManager;
import org.hibernate.search.jpa.FullTextQuery;
import org.hibernate.search.query.dsl.QueryBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.movieadvisor.bo.Suggestion;
import com.movieadvisor.repository.dao.GenreDAO;
import com.movieadvisor.repository.domain.Genre;
import com.movieadvisor.repository.domain.Movie;

@Service
@Transactional
public class GenreServiceImpl extends AbstractDatabaseService<Genre, Long> implements GenreService {

	private static final Logger LOGGER = LoggerFactory.getLogger(GenreServiceImpl.class);
	
	private static final String NAME_EDGE_NGRAM_INDEX = "edgeNGramName";
	private static final String NAME_NGRAM_INDEX = "nGramName";		
	
	@Autowired
	private GenreDAO genreDAO;
	
	@Autowired
	private FullTextEntityManager fullTextEntityManager;	

	@Override
	protected CrudRepository<Genre, Long> getRepository() {
		return genreDAO;
	}

	@Override
	protected Genre buildDomain() {
		Genre genre = new Genre();		
				
		String name = RandomStringUtils.randomAlphabetic(20);		
		int random = RandomUtils.nextInt(11) + 1;
		
		if (random > 5) {
			name = StringUtils.replace(name, Character.toString(name.charAt(random)), " ");
		}
		
		genre.setName(name);
		
		return genre;
	}

	@Override
	public void initRandomData(int size) {

		InputStream in = this.getClass().getClassLoader().getResourceAsStream("genres.txt");		
		List<String> genres;
		try {
			genres = IOUtils.readLines(in);
		} catch (IOException e) {
			throw new IllegalStateException(e);
		}
		
		Genre genre;
		int i = 0;
		while ( i < size ) {			
			String g = genres.get(i);
			
			try {
				genre = new Genre();
				
				genre.setName(g);
				
				genreDAO.saveAndFlush(genre);
				
				i++;
			} catch (Throwable t) {
				LOGGER.error(ExceptionUtils.getMessage(t));
			}
		}
		
		while (i < size) {
			try {
				genreDAO.saveAndFlush(buildDomain());
				i++;
			} catch (Throwable t) {
				LOGGER.error(ExceptionUtils.getMessage(t));
			}
		}		
	}

	@Override
	public List<Suggestion> suggest(String term) {
		LOGGER.info("searching suggestions : " + term);
				
		QueryBuilder titleQB = fullTextEntityManager.getSearchFactory()
				.buildQueryBuilder().forEntity(Genre.class).get();
		
		Query query = titleQB.phrase().withSlop(2)
				.onField(NAME_NGRAM_INDEX)
				.andField(NAME_EDGE_NGRAM_INDEX).boostedTo(5)
				.sentence(term.toLowerCase())
				.createQuery();
		
		FullTextQuery fullTextQuery = fullTextEntityManager.createFullTextQuery(query, Genre.class);
		
		fullTextQuery.setProjection("name", FullTextQuery.SCORE);
		fullTextQuery.setMaxResults(20);
		
		@SuppressWarnings("unchecked")
		List<Object[]> list = fullTextQuery.getResultList();
		
		List<Suggestion> result = new ArrayList<>(list.size());
		for (Object[] obj : list) {
			Suggestion suggestion = new Suggestion();
			
			suggestion.setText(String.valueOf(obj[0]));
			suggestion.setScore((Float) obj[1]);

			result.add(suggestion);
		}
		
		LOGGER.info("searched suggestions");
		
		return result;
	}
		
	@Override
	public void initMovieConnections(Long minMovieId, Long maxMovieId, int size) {
		if (minMovieId == null || maxMovieId == null) {
			return;
		}
		
		List<Genre> genres = genreDAO.findAll();
		
		int connectionsSize = RandomUtils.nextInt(11) + 1;		
		
		Set<Long> selectedIds = null;				
		Set<Movie> movies = null;
		Movie movie = null;
		Long movieId = null;
		
		int i = 0;
		while ( i < genres.size() && i < size ) {			
			Genre genre = genres.get(i);
			try {
				selectedIds = new HashSet<>();
				movies = new HashSet<>();
				
				for (int j = 0; j < connectionsSize; j++) {
					movie = new Movie();
	
					do {
					    long range = maxMovieId - minMovieId + 1;
					    long fraction = (long) (range * RandomUtils.nextDouble());
					    
					    movieId = fraction + minMovieId;
					} while (selectedIds.contains(movieId));
					
					selectedIds.add(movieId);
					
					movie.setId(movieId);				
					movies.add(movie);
				}
				
				genre.getMovies().addAll(movies);
				
				genreDAO.saveAndFlush(genre);
				
				i++;
			} catch (Throwable t) {
				LOGGER.error(ExceptionUtils.getMessage(t));
			}
		}		
	}	
}
