package com.movieadvisor.service;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import org.apache.commons.lang.exception.ExceptionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service(value = "dataService")
public class DataServiceImpl implements DataService {

	private static final Logger LOGGER = LoggerFactory.getLogger(DataServiceImpl.class);
	
	@Autowired
	private ActorService actorService;
	
	@Autowired
	private GenreService genreService;
	
	@Autowired
	private MovieService movieService;
	
	@Override
	public void initRandomData(final int size) {
		LOGGER.info("start data initialization ...");
		
		long startTime = System.currentTimeMillis();
		
		ExecutorService service = Executors.newFixedThreadPool(3);
		
		loadData(service, genreService, size);
		loadData(service, actorService, size);
		loadData(service, movieService, size);
		
		service.shutdown();

        try {
			service.awaitTermination(Long.MAX_VALUE, TimeUnit.SECONDS);
		} catch (InterruptedException e) {
			LOGGER.error(ExceptionUtils.getStackTrace(e));
		}
        
        if (service.isTerminated()) {
    		final Long movieMinId = movieService.findMinId();
    		final Long movieMaxId = movieService.findMaxId();        	
        	
        	initMovieConnections(movieMinId, movieMaxId, size);
        }

        LOGGER.info("data initialized");
        
        LOGGER.info("Duration (" + size + ") " + (System.currentTimeMillis() - startTime));
	}
	
	public void initMovieConnections(final Long minMovieId, final Long maxMovieId, final int size) {
		LOGGER.info("initializing data connections ...");		
		
		ExecutorService service = Executors.newFixedThreadPool(3);
		
		service.execute(new Runnable() {			
			@Override
			public void run() {
				actorService.initMovieConnections(minMovieId, maxMovieId, size);
			}
		});
		
		service.execute(new Runnable() {			
			@Override
			public void run() {
				genreService.initMovieConnections(minMovieId, maxMovieId, size);
			}
		});
		
		service.shutdown();

        try {
			service.awaitTermination(Long.MAX_VALUE, TimeUnit.SECONDS);
		} catch (InterruptedException e) {
			LOGGER.error(ExceptionUtils.getStackTrace(e));
		}

        if (service.isTerminated()) {
        	LOGGER.info("initializing data connections done");
        }		
	}
	
	private void loadData(ExecutorService service, final DataService dataService, final int size) {
		service.execute(new Runnable() {			
			@Override
			public void run() {
				dataService.initRandomData(size);
			}
		});
	}
}
