package com.movieadvisor.service;


public interface DataService {
	void initRandomData(int size);
	
	void initMovieConnections(Long minMovieId, Long maxMovieId, int size);	
}
