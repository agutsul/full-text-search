package com.movieadvisor.service;

import com.movieadvisor.repository.domain.Genre;

public interface GenreService extends DatabaseService<Genre, Long>, SuggestionService {

}
