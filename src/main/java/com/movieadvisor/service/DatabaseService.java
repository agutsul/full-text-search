package com.movieadvisor.service;

import java.io.Serializable;

public interface DatabaseService<T, ID extends Serializable> extends DataService {
	T persist(T movie);
	
	T update(T movie);
	
	T findById(ID id);
	
	void delete(ID id);
}
