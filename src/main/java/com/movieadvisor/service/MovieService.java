package com.movieadvisor.service;

import java.util.List;

import com.movieadvisor.repository.domain.Movie;

public interface MovieService extends DatabaseService<Movie, Long>, SuggestionService {
	Long findMaxId();
	
	Long findMinId();
	
	List<com.movieadvisor.bo.Movie> search(String term);
}
