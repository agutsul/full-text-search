package com.movieadvisor.service;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.RandomStringUtils;
import org.apache.commons.lang.exception.ExceptionUtils;
import org.apache.commons.lang.math.RandomUtils;
import org.apache.lucene.search.Query;
import org.hibernate.search.jpa.FullTextEntityManager;
import org.hibernate.search.jpa.FullTextQuery;
import org.hibernate.search.query.dsl.QueryBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.movieadvisor.bo.Suggestion;
import com.movieadvisor.repository.dao.ActorDAO;
import com.movieadvisor.repository.domain.Actor;
import com.movieadvisor.repository.domain.Movie;

@Service
@Transactional
public class ActorServiceImpl extends AbstractDatabaseService<Actor, Long> implements ActorService {

	private static final Logger LOGGER = LoggerFactory.getLogger(ActorServiceImpl.class);
	
	private static final String FIRST_NAME_EDGE_NGRAM_INDEX = "edgeNGramFirstName";
	private static final String FIRST_NAME_NGRAM_INDEX = "nGramFirstName";	

	private static final String LAST_NAME_EDGE_NGRAM_INDEX = "edgeNGramFirstName";
	private static final String LAST_NAME_NGRAM_INDEX = "nGramLastName";	
	
	@Autowired
	private ActorDAO actorDAO;
	
	@Autowired
	private FullTextEntityManager fullTextEntityManager;

	@Override
	protected CrudRepository<Actor, Long> getRepository() {
		return actorDAO;
	}

	@Override
	protected Actor buildDomain() {
		Actor actor = new Actor();
		
		actor.setFirstName(RandomStringUtils.randomAlphabetic(20));
		actor.setLastName(RandomStringUtils.randomAlphabetic(25));
		
		return actor;
	}
	
	@Override
	public void initRandomData(int size) {

		InputStream in = this.getClass().getClassLoader().getResourceAsStream("actors.txt");		
		List<String> actors;
		try {
			actors = IOUtils.readLines(in);
		} catch (IOException e) {
			throw new IllegalStateException(e);
		}
		
		Actor a;
		String[] actorsData;
		int i = 0;
		
		while ( i < actors.size() && i < size ){
			String actor = actors.get(i);
			try {				
				a = new Actor();
				actorsData = actor.trim().split("\\s");
				
				a.setFirstName(actorsData[0]);
				a.setLastName(actorsData[1]);
			
				actorDAO.saveAndFlush(a);
				
				i++;
			} catch (Throwable t) {
				LOGGER.error(ExceptionUtils.getMessage(t));
			}
		}
		
		while ( i < size ) {
			try {
				actorDAO.saveAndFlush(buildDomain());
				i++;
			} catch (Throwable t) {
				LOGGER.error(ExceptionUtils.getMessage(t));
			}
		}
	}

	
	@Override
	public void initMovieConnections(Long minMovieId, Long maxMovieId, int size) {
		if (minMovieId == null || maxMovieId == null) {
			return;
		}
		
		List<Actor> actors = actorDAO.findAll();
		
		 
		
		int connectionsSize = RandomUtils.nextInt(10) + 1;		
		Set<Long> selectedIds = new HashSet<>();		
		
		Set<Movie> movies = new HashSet<>();
		Movie movie = null;
		Long movieId = null;
		
		for (Actor actor : actors) {
			try {
				selectedIds.clear();
				movies.clear();
				
				for (int i = 0; i < connectionsSize; i++) {
					movie = new Movie();
	
					do {
					    long range = maxMovieId - minMovieId + 1;
					    long fraction = (long) (range * RandomUtils.nextDouble());
					    
					    movieId = fraction + minMovieId;  					
					} while(selectedIds.contains(movieId));
					
					selectedIds.add(movieId);
					
					movie.setId(movieId);				
					movies.add(movie);
				}
				
				
				actor.getMovies().addAll(movies);
				
				actorDAO.saveAndFlush(actor);
			} catch (Throwable t) {
				LOGGER.error(ExceptionUtils.getMessage(t));
			}
		}		
	}

	@Override
	public List<Suggestion> suggest(String term) {
		LOGGER.info("searching suggestions : " + term);
		
		List<Suggestion> actorsByFirstName = suggest(term, FIRST_NAME_EDGE_NGRAM_INDEX, FIRST_NAME_NGRAM_INDEX);
		List<Suggestion> actorsByLastName = suggest(term, LAST_NAME_EDGE_NGRAM_INDEX, LAST_NAME_NGRAM_INDEX);
		
		Set<Suggestion> suggestions = new TreeSet<>();
		
		suggestions.addAll(actorsByFirstName);
		suggestions.addAll(actorsByLastName);
		
		LOGGER.info("searched suggestions");
		
		return new ArrayList<>(suggestions);
	}
		
	private List<Suggestion> suggest(String term, String fieldEdgeNGram, String fieldNGramIndex) {
		QueryBuilder titleQB = fullTextEntityManager.getSearchFactory()
				.buildQueryBuilder().forEntity(Actor.class).get();
		
		Query query = titleQB.phrase().withSlop(2)
				.onField(fieldEdgeNGram)
				.andField(fieldNGramIndex).boostedTo(5)
				.sentence(term.toLowerCase())
				.createQuery();
		
		FullTextQuery fullTextQuery = fullTextEntityManager.createFullTextQuery(query, Actor.class);
		
		fullTextQuery.setProjection("firstName","lastName",FullTextQuery.SCORE);
		fullTextQuery.setMaxResults(20);
		
		@SuppressWarnings("unchecked")
		List<Object[]> list = fullTextQuery.getResultList();
		
		List<Suggestion> suggestions = new ArrayList<>(list.size());
		for (Object[] obj : list) {
			Suggestion suggestion = new Suggestion();
			
			suggestion.setText(String.format("%s %s", obj[0], obj[1]));
			suggestion.setScore((Float) obj[2]);
			
			suggestions.add(suggestion);
		}
		
		return suggestions;
	}

}
