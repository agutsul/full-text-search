package com.movieadvisor.web.aop;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

@Aspect
@Component
public class ControllerLoggerAspect {

	private static final Logger LOGGER = LoggerFactory.getLogger(ControllerLoggerAspect.class);
	
	private static final String METHOD_STARTED_TEMPLATE = "{}.{}({}) started";
	private static final String METHOD_FINISHED_TEMPLATE = "{}.{} ({} ms) finished";
	private static final String METHOD_RETURN_TEMPLATE = "Returning: {}";		
		
	@Around("execution(* com.movieadvisor.controller..*Controller.*(..))")
	public Object aroundMethod(ProceedingJoinPoint joinPoint) throws Throwable {
		String className = joinPoint.getTarget().getClass().getSimpleName();
		String methodName = joinPoint.getSignature().getName();		
		Object[] args = joinPoint.getArgs();
		
		StringBuilder paramsBuilder = new StringBuilder();
		for (int i = 0; i < args.length; i++) {
			paramsBuilder.append(String.valueOf(args[i]));
			
			if (i < args.length - 1) {
				paramsBuilder.append(",");
			}
		}		
		
		LOGGER.info(METHOD_STARTED_TEMPLATE, className, methodName, paramsBuilder);
		
		Object result = null;
		long startTime = System.currentTimeMillis();
		try {
			result = joinPoint.proceed(args);
			LOGGER.info(METHOD_RETURN_TEMPLATE, result);
		} finally {
			long duration = System.currentTimeMillis() - startTime;

			LOGGER.info(METHOD_FINISHED_TEMPLATE, className, methodName, duration);
		}		
		
		return result;		
	}	
}