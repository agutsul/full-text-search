package com.movieadvisor.web.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.movieadvisor.bo.Suggestion;
import com.movieadvisor.bo.Movie;
import com.movieadvisor.service.MovieService;

@Controller
@RequestMapping(value = "/movie")
public class MovieController {

	@Autowired
	private MovieService movieService;
	    
    @RequestMapping(value = "/suggest", method = RequestMethod.GET, params = {"term"})
    public ResponseEntity<List<Suggestion>> suggest(@RequestParam("term") String term) {
    	return new ResponseEntity<List<Suggestion>>(movieService.suggest(term), HttpStatus.OK);
    }
    
    @RequestMapping(value = "/search", method = RequestMethod.GET, params = {"term"})
    public ResponseEntity<List<Movie>> search(@RequestParam("term") String term) {    	
    	return new ResponseEntity<List<Movie>>(movieService.search(term), HttpStatus.OK);
    }
    
}
