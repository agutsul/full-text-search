package com.movieadvisor.web.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;

import com.movieadvisor.service.DataService;

@Controller
@RequestMapping(value = "/data")
public class DataController {

	@Autowired
	@Qualifier(value = "dataService")
	private DataService dataService;
	
	@RequestMapping(value = "/init", method = RequestMethod.GET, params = {"size"})
	@ResponseStatus(HttpStatus.OK)
	public void initData(@RequestParam("size") int size) {
		dataService.initRandomData(size);
	}
}
