package com.movieadvisor.repository.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.movieadvisor.repository.domain.Movie;

@Repository
public interface MovieDAO extends JpaRepository<Movie, Long> {

	@Query("select max(a.id) from Movie a")
	Long getMaxId();
	
	@Query("select min(a.id) from Movie a")
	Long getMinId();

}
