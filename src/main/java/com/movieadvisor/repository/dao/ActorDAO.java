package com.movieadvisor.repository.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.movieadvisor.repository.domain.Actor;

@Repository
public interface ActorDAO extends JpaRepository<Actor, Long> {
	
}
