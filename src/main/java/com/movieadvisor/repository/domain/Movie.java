package com.movieadvisor.repository.domain;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.solr.analysis.EdgeNGramFilterFactory;
import org.apache.solr.analysis.KeywordTokenizerFactory;
import org.apache.solr.analysis.LowerCaseFilterFactory;
import org.apache.solr.analysis.NGramFilterFactory;
import org.apache.solr.analysis.PatternReplaceFilterFactory;
import org.apache.solr.analysis.StandardTokenizerFactory;
import org.apache.solr.analysis.StopFilterFactory;
import org.apache.solr.analysis.WordDelimiterFilterFactory;
import org.hibernate.search.annotations.Analyze;
import org.hibernate.search.annotations.Analyzer;
import org.hibernate.search.annotations.AnalyzerDef;
import org.hibernate.search.annotations.AnalyzerDefs;
import org.hibernate.search.annotations.DocumentId;
import org.hibernate.search.annotations.Field;
import org.hibernate.search.annotations.Fields;
import org.hibernate.search.annotations.Index;
import org.hibernate.search.annotations.Indexed;
import org.hibernate.search.annotations.IndexedEmbedded;
import org.hibernate.search.annotations.Parameter;
import org.hibernate.search.annotations.Store;
import org.hibernate.search.annotations.TokenFilterDef;
import org.hibernate.search.annotations.TokenizerDef;

@Entity
@Indexed
//@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@AnalyzerDefs({
	@AnalyzerDef(name = "autocompleteEdgeAnalyzer",
	// Split input into tokens according to tokenizer
	tokenizer = @TokenizerDef(factory = KeywordTokenizerFactory.class),
	filters = {
		// Normalize token text to lowercase, as the user is unlikely to
		// care about casing when searching for matches
		@TokenFilterDef(factory = PatternReplaceFilterFactory.class, params = {
			@Parameter(name = "pattern",value = "([^a-zA-Z0-9\\.])"),
			@Parameter(name = "replacement", value = " "),
			@Parameter(name = "replace", value = "all") }),
		@TokenFilterDef(factory = LowerCaseFilterFactory.class),
		@TokenFilterDef(factory = StopFilterFactory.class),
		// Index partial words starting at the front, so we can provide
		// Autocomplete functionality
		@TokenFilterDef(factory = EdgeNGramFilterFactory.class, params = {
			@Parameter(name = "minGramSize", value = "3"),
			@Parameter(name = "maxGramSize", value = "50") }) 
	}),
	@AnalyzerDef(name = "autocompleteNGramAnalyzer",
	// Split input into tokens according to tokenizer
	tokenizer = @TokenizerDef(factory = StandardTokenizerFactory.class),
	filters = {
		// Normalize token text to lowercase, as the user is unlikely to
		// care about casing when searching for matches
		@TokenFilterDef(factory = WordDelimiterFilterFactory.class),
		@TokenFilterDef(factory = LowerCaseFilterFactory.class),
		@TokenFilterDef(factory = NGramFilterFactory.class, params = {
			@Parameter(name = "minGramSize", value = "3"),
			@Parameter(name = "maxGramSize", value = "5") }),
		@TokenFilterDef(factory = PatternReplaceFilterFactory.class, params = {
			@Parameter(name = "pattern",value = "([^a-zA-Z0-9\\.])"),
			@Parameter(name = "replacement", value = " "),
			@Parameter(name = "replace", value = "all") })
	}),
	@AnalyzerDef(name = "standardMovieAnalyzer",
	// Split input into tokens according to tokenizer
	tokenizer = @TokenizerDef(factory = StandardTokenizerFactory.class),
	filters = {
		// Normalize token text to lowercase, as the user is unlikely to
		// care about casing when searching for matches
		@TokenFilterDef(factory = WordDelimiterFilterFactory.class),
		@TokenFilterDef(factory = LowerCaseFilterFactory.class),
		@TokenFilterDef(factory = PatternReplaceFilterFactory.class, params = {
		@Parameter(name = "pattern", value = "([^a-zA-Z0-9\\.])"),
		@Parameter(name = "replacement", value = " "),
		@Parameter(name = "replace", value = "all") })
	})
})
@SequenceGenerator(name = "movie_seq", sequenceName = "movie_id_seq", allocationSize = 1)
@Table(name = "movie", uniqueConstraints = @UniqueConstraint(columnNames = {"title", "director_name" }))
public class Movie implements java.io.Serializable {

	private static final long serialVersionUID = -333301238572873928L;

	private long id;
	private String title;
	private String directorName;
	private Integer rating;
	private Set<Genre> genres = new HashSet<Genre>(0);
	private Set<Actor> actors = new HashSet<Actor>(0);

	public Movie() {
	}

	public Movie(long id, String title, String directorName) {
		this.id = id;
		this.title = title;
		this.directorName = directorName;
	}

	public Movie(long id, String title, String directorName, Integer rating,
			Set<Genre> genres, Set<Actor> actors) {
		this.id = id;
		this.title = title;
		this.directorName = directorName;
		this.rating = rating;
		this.genres = genres;
		this.actors = actors;
	}

	@Id
	@DocumentId
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "movie_seq")
	@Column(name = "id", unique = true, nullable = false)
	public long getId() {
		return this.id;
	}

	public void setId(long id) {
		this.id = id;
	}

	@Fields({
		@Field(name = "title", index = Index.YES, store = Store.YES,
				analyze = Analyze.YES, analyzer = @Analyzer(definition = "standardMovieAnalyzer")),
		@Field(name = "edgeNGramTitle", index = Index.YES, store = Store.NO,
			analyze = Analyze.YES, analyzer = @Analyzer(definition = "autocompleteEdgeAnalyzer")),
		@Field(name = "nGramTitle", index = Index.YES, store = Store.NO,
			analyze = Analyze.YES, analyzer = @Analyzer(definition = "autocompleteNGramAnalyzer"))
		})	
	@Column(name = "title", nullable = false, length = 100)
	public String getTitle() {
		return this.title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	@Field(name = "directorName", store = Store.YES) 
	@Column(name = "director_name", nullable = false, length = 50)
	public String getDirectorName() {
		return this.directorName;
	}

	public void setDirectorName(String directorName) {
		this.directorName = directorName;
	}

	@Field(name = "rating", store = Store.YES)
	@Column(name = "rating")
	public Integer getRating() {
		return this.rating;
	}

	public void setRating(Integer rating) {
		this.rating = rating;
	}

	@IndexedEmbedded(depth = 1)
	@ManyToMany(fetch = FetchType.LAZY)
	@JoinTable(name = "movie_genre", 
		joinColumns = { @JoinColumn(name = "movie_id", nullable = false, updatable = false) } , 
		inverseJoinColumns = { @JoinColumn(name = "genre_id", nullable = false, updatable = false) })
	public Set<Genre> getGenres() {
		return this.genres;
	}

	public void setGenres(Set<Genre> genres) {
		this.genres = genres;
	}

	@IndexedEmbedded(depth = 1)
	@ManyToMany(fetch = FetchType.LAZY)
	@JoinTable(name = "movie_actor", 
		joinColumns = { @JoinColumn(name = "movie_id", nullable = false, updatable = false)} , 
		inverseJoinColumns = { @JoinColumn(name = "actor_id", nullable = false, updatable = false) } )
	public Set<Actor> getActors() {
		return this.actors;
	}

	public void setActors(Set<Actor> actors) {
		this.actors = actors;
	}

	@Override
	public int hashCode() {
		return HashCodeBuilder.reflectionHashCode(this, false);
	}

	@Override
	public boolean equals(Object obj) {
		return EqualsBuilder.reflectionEquals(this, obj, false);
	}
}
