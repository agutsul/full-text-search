package com.movieadvisor.repository.domain;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.solr.analysis.EdgeNGramFilterFactory;
import org.apache.solr.analysis.KeywordTokenizerFactory;
import org.apache.solr.analysis.LowerCaseFilterFactory;
import org.apache.solr.analysis.NGramFilterFactory;
import org.apache.solr.analysis.PatternReplaceFilterFactory;
import org.apache.solr.analysis.StandardTokenizerFactory;
import org.apache.solr.analysis.StopFilterFactory;
import org.apache.solr.analysis.WordDelimiterFilterFactory;
import org.hibernate.search.annotations.Analyze;
import org.hibernate.search.annotations.Analyzer;
import org.hibernate.search.annotations.AnalyzerDef;
import org.hibernate.search.annotations.AnalyzerDefs;
import org.hibernate.search.annotations.ContainedIn;
import org.hibernate.search.annotations.DocumentId;
import org.hibernate.search.annotations.Field;
import org.hibernate.search.annotations.Fields;
import org.hibernate.search.annotations.Index;
import org.hibernate.search.annotations.Indexed;
import org.hibernate.search.annotations.Parameter;
import org.hibernate.search.annotations.Store;
import org.hibernate.search.annotations.TokenFilterDef;
import org.hibernate.search.annotations.TokenizerDef;

@Entity
@Indexed
//@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@AnalyzerDefs({
	@AnalyzerDef(name = "autocompleteFirstNameNGramAnalyzer",
	// Split input into tokens according to tokenizer
	tokenizer = @TokenizerDef(factory = KeywordTokenizerFactory.class),
	filters = {
		// Normalize token text to lowercase, as the user is unlikely to
		// care about casing when searching for matches
		@TokenFilterDef(factory = PatternReplaceFilterFactory.class, params = {
			@Parameter(name = "pattern",value = "([^a-zA-Z0-9\\.])"),
			@Parameter(name = "replacement", value = " "),
			@Parameter(name = "replace", value = "all") }),
		@TokenFilterDef(factory = LowerCaseFilterFactory.class),
		@TokenFilterDef(factory = StopFilterFactory.class),
		// Index partial words starting at the front, so we can provide
		// Autocomplete functionality
		@TokenFilterDef(factory = EdgeNGramFilterFactory.class, params = {
			@Parameter(name = "minGramSize", value = "3"),
			@Parameter(name = "maxGramSize", value = "50") }) 
	}),
	@AnalyzerDef(name = "autocompleteFirstNameEdgeAnalyzer",
	// Split input into tokens according to tokenizer
	tokenizer = @TokenizerDef(factory = StandardTokenizerFactory.class),
	filters = {
		// Normalize token text to lowercase, as the user is unlikely to
		// care about casing when searching for matches
		@TokenFilterDef(factory = WordDelimiterFilterFactory.class),
		@TokenFilterDef(factory = LowerCaseFilterFactory.class),
		@TokenFilterDef(factory = NGramFilterFactory.class, params = {
			@Parameter(name = "minGramSize", value = "3"),
			@Parameter(name = "maxGramSize", value = "5") }),
		@TokenFilterDef(factory = PatternReplaceFilterFactory.class, params = {
			@Parameter(name = "pattern",value = "([^a-zA-Z0-9\\.])"),
			@Parameter(name = "replacement", value = " "),
			@Parameter(name = "replace", value = "all") })
	}),
	@AnalyzerDef(name = "standardFirstNameAnalyzer",
	// Split input into tokens according to tokenizer
	tokenizer = @TokenizerDef(factory = StandardTokenizerFactory.class),
	filters = {
		// Normalize token text to lowercase, as the user is unlikely to
		// care about casing when searching for matches
		@TokenFilterDef(factory = WordDelimiterFilterFactory.class),
		@TokenFilterDef(factory = LowerCaseFilterFactory.class),
		@TokenFilterDef(factory = PatternReplaceFilterFactory.class, params = {
		@Parameter(name = "pattern", value = "([^a-zA-Z0-9\\.])"),
		@Parameter(name = "replacement", value = " "),
		@Parameter(name = "replace", value = "all") })
	}),
	@AnalyzerDef(name = "autocompleteLastNameNGramAnalyzer",
	// Split input into tokens according to tokenizer
	tokenizer = @TokenizerDef(factory = KeywordTokenizerFactory.class),
	filters = {
		// Normalize token text to lowercase, as the user is unlikely to
		// care about casing when searching for matches
		@TokenFilterDef(factory = PatternReplaceFilterFactory.class, params = {
			@Parameter(name = "pattern",value = "([^a-zA-Z0-9\\.])"),
			@Parameter(name = "replacement", value = " "),
			@Parameter(name = "replace", value = "all") }),
		@TokenFilterDef(factory = LowerCaseFilterFactory.class),
		@TokenFilterDef(factory = StopFilterFactory.class),
		// Index partial words starting at the front, so we can provide
		// Autocomplete functionality
		@TokenFilterDef(factory = EdgeNGramFilterFactory.class, params = {
			@Parameter(name = "minGramSize", value = "3"),
			@Parameter(name = "maxGramSize", value = "50") }) 
	}),
	@AnalyzerDef(name = "autocompleteLastNameEdgeAnalyzer",
	// Split input into tokens according to tokenizer
	tokenizer = @TokenizerDef(factory = StandardTokenizerFactory.class),
	filters = {
		// Normalize token text to lowercase, as the user is unlikely to
		// care about casing when searching for matches
		@TokenFilterDef(factory = WordDelimiterFilterFactory.class),
		@TokenFilterDef(factory = LowerCaseFilterFactory.class),
		@TokenFilterDef(factory = NGramFilterFactory.class, params = {
			@Parameter(name = "minGramSize", value = "3"),
			@Parameter(name = "maxGramSize", value = "5") }),
		@TokenFilterDef(factory = PatternReplaceFilterFactory.class, params = {
			@Parameter(name = "pattern",value = "([^a-zA-Z0-9\\.])"),
			@Parameter(name = "replacement", value = " "),
			@Parameter(name = "replace", value = "all") })
	}),
	@AnalyzerDef(name = "standardLastNameAnalyzer",
	// Split input into tokens according to tokenizer
	tokenizer = @TokenizerDef(factory = StandardTokenizerFactory.class),
	filters = {
		// Normalize token text to lowercase, as the user is unlikely to
		// care about casing when searching for matches
		@TokenFilterDef(factory = WordDelimiterFilterFactory.class),
		@TokenFilterDef(factory = LowerCaseFilterFactory.class),
		@TokenFilterDef(factory = PatternReplaceFilterFactory.class, params = {
		@Parameter(name = "pattern", value = "([^a-zA-Z0-9\\.])"),
		@Parameter(name = "replacement", value = " "),
		@Parameter(name = "replace", value = "all") })
	})	
})
@SequenceGenerator(name = "actor_seq", sequenceName = "actor_id_seq", allocationSize = 1)
@Table(name = "actor", uniqueConstraints = @UniqueConstraint(columnNames = {"first_name", "last_name" }))
public class Actor implements java.io.Serializable {

	private static final long serialVersionUID = 6759633471232717540L;

	private long id;
	private String firstName;
	private String lastName;
	private Set<Movie> movies = new HashSet<Movie>(0);


	
	public Actor() {
	}

	public Actor(long id, String firstName, String lastName) {
		this.id = id;
		this.firstName = firstName;
		this.lastName = lastName;
	}

	public Actor(long id, String firstName, String lastName, Set<Movie> movies) {
		this.id = id;
		this.firstName = firstName;
		this.lastName = lastName;
		this.movies = movies;
	}

	@Id
	@DocumentId
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "actor_seq")
	@Column(name = "id", unique = true, nullable = false)
	public long getId() {
		return this.id;
	}

	public void setId(long id) {
		this.id = id;
	}

	@Fields({
		@Field(name = "firstName", index = Index.YES, store = Store.YES,
				analyze = Analyze.YES, analyzer = @Analyzer(definition = "standardFirstNameAnalyzer")),
		@Field(name = "edgeNGramFirstName", index = Index.YES, store = Store.NO,
			analyze = Analyze.YES, analyzer = @Analyzer(definition = "autocompleteFirstNameEdgeAnalyzer")),
		@Field(name = "nGramFirstName", index = Index.YES, store = Store.NO,
			analyze = Analyze.YES, analyzer = @Analyzer(definition = "autocompleteFirstNameNGramAnalyzer"))
		})
	@Column(name = "first_name", nullable = false, length = 20)
	public String getFirstName() {
		return this.firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	@Fields({
		@Field(name = "lastName", index = Index.YES, store = Store.YES,
				analyze = Analyze.YES, analyzer = @Analyzer(definition = "standardLastNameAnalyzer")),
		@Field(name = "edgeNGramLastName", index = Index.YES, store = Store.NO,
			analyze = Analyze.YES, analyzer = @Analyzer(definition = "autocompleteLastNameEdgeAnalyzer")),
		@Field(name = "nGramLastName", index = Index.YES, store = Store.NO,
			analyze = Analyze.YES, analyzer = @Analyzer(definition = "autocompleteLastNameNGramAnalyzer"))
		})
	@Column(name = "last_name", nullable = false, length = 25)
	public String getLastName() {
		return this.lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	@ContainedIn
	@ManyToMany(fetch = FetchType.LAZY)
	@JoinTable(name = "movie_actor", 
		joinColumns = { @JoinColumn(name = "actor_id", nullable = false, updatable = false) }, 
		inverseJoinColumns = { @JoinColumn(name = "movie_id", nullable = false, updatable = false) })
	public Set<Movie> getMovies() {
		return this.movies;
	}

	public void setMovies(Set<Movie> movies) {
		this.movies = movies;
	}

	@Override
	public int hashCode() {
		return HashCodeBuilder.reflectionHashCode(this, false);
	}

	@Override
	public boolean equals(Object obj) {
		return EqualsBuilder.reflectionEquals(this, obj, false);
	}

}
