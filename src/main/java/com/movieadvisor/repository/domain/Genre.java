package com.movieadvisor.repository.domain;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.solr.analysis.EdgeNGramFilterFactory;
import org.apache.solr.analysis.KeywordTokenizerFactory;
import org.apache.solr.analysis.LowerCaseFilterFactory;
import org.apache.solr.analysis.NGramFilterFactory;
import org.apache.solr.analysis.PatternReplaceFilterFactory;
import org.apache.solr.analysis.StandardTokenizerFactory;
import org.apache.solr.analysis.StopFilterFactory;
import org.apache.solr.analysis.WordDelimiterFilterFactory;
import org.hibernate.search.annotations.Analyze;
import org.hibernate.search.annotations.Analyzer;
import org.hibernate.search.annotations.AnalyzerDef;
import org.hibernate.search.annotations.AnalyzerDefs;
import org.hibernate.search.annotations.ContainedIn;
import org.hibernate.search.annotations.DocumentId;
import org.hibernate.search.annotations.Field;
import org.hibernate.search.annotations.Fields;
import org.hibernate.search.annotations.Index;
import org.hibernate.search.annotations.Indexed;
import org.hibernate.search.annotations.Parameter;
import org.hibernate.search.annotations.Store;
import org.hibernate.search.annotations.TokenFilterDef;
import org.hibernate.search.annotations.TokenizerDef;

@Entity
@Indexed
//@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@AnalyzerDefs({
	@AnalyzerDef(name = "autocompleteGenreEdgeAnalyzer",
	// Split input into tokens according to tokenizer
	tokenizer = @TokenizerDef(factory = KeywordTokenizerFactory.class),
	filters = {
		// Normalize token text to lowercase, as the user is unlikely to
		// care about casing when searching for matches
		@TokenFilterDef(factory = PatternReplaceFilterFactory.class, params = {
			@Parameter(name = "pattern",value = "([^a-zA-Z0-9\\.])"),
			@Parameter(name = "replacement", value = " "),
			@Parameter(name = "replace", value = "all") }),
		@TokenFilterDef(factory = LowerCaseFilterFactory.class),
		@TokenFilterDef(factory = StopFilterFactory.class),
		// Index partial words starting at the front, so we can provide
		// Autocomplete functionality
		@TokenFilterDef(factory = EdgeNGramFilterFactory.class, params = {
			@Parameter(name = "minGramSize", value = "3"),
			@Parameter(name = "maxGramSize", value = "50") }) 
	}),
	@AnalyzerDef(name = "autocompleteGenreNGramAnalyzer",
	// Split input into tokens according to tokenizer
	tokenizer = @TokenizerDef(factory = StandardTokenizerFactory.class),
	filters = {
		// Normalize token text to lowercase, as the user is unlikely to
		// care about casing when searching for matches
		@TokenFilterDef(factory = WordDelimiterFilterFactory.class),
		@TokenFilterDef(factory = LowerCaseFilterFactory.class),
		@TokenFilterDef(factory = NGramFilterFactory.class, params = {
			@Parameter(name = "minGramSize", value = "3"),
			@Parameter(name = "maxGramSize", value = "5") }),
		@TokenFilterDef(factory = PatternReplaceFilterFactory.class, params = {
			@Parameter(name = "pattern",value = "([^a-zA-Z0-9\\.])"),
			@Parameter(name = "replacement", value = " "),
			@Parameter(name = "replace", value = "all") })
	}),
	@AnalyzerDef(name = "standardGenreAnalyzer",
	// Split input into tokens according to tokenizer
	tokenizer = @TokenizerDef(factory = StandardTokenizerFactory.class),
	filters = {
		// Normalize token text to lowercase, as the user is unlikely to
		// care about casing when searching for matches
		@TokenFilterDef(factory = WordDelimiterFilterFactory.class),
		@TokenFilterDef(factory = LowerCaseFilterFactory.class),
		@TokenFilterDef(factory = PatternReplaceFilterFactory.class, params = {
		@Parameter(name = "pattern", value = "([^a-zA-Z0-9\\.])"),
		@Parameter(name = "replacement", value = " "),
		@Parameter(name = "replace", value = "all") })
	})
})

@SequenceGenerator(name = "genre_seq", sequenceName = "genre_id_seq", allocationSize = 1)
@Table(name = "genre", uniqueConstraints = @UniqueConstraint(columnNames = "name"))
public class Genre implements java.io.Serializable {

	private static final long serialVersionUID = -1584168277463309210L;

	private long id;
	private String name;
	private Set<Movie> movies = new HashSet<Movie>(0);

	public Genre() {
	}

	public Genre(long id, String name) {
		this.id = id;
		this.name = name;
	}

	public Genre(long id, String name, Set<Movie> movies) {
		this.id = id;
		this.name = name;
		this.movies = movies;
	}

	@Id
	@DocumentId
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "genre_seq")
	@Column(name = "id", unique = true, nullable = false)
	public long getId() {
		return this.id;
	}

	public void setId(long id) {
		this.id = id;
	}

	@Fields({
		@Field(name = "name", index = Index.YES, store = Store.YES,
				analyze = Analyze.YES, analyzer = @Analyzer(definition = "standardGenreAnalyzer")),
		@Field(name = "edgeNGramName", index = Index.YES, store = Store.NO,
			analyze = Analyze.YES, analyzer = @Analyzer(definition = "autocompleteGenreEdgeAnalyzer")),
		@Field(name = "nGramName", index = Index.YES, store = Store.NO,
			analyze = Analyze.YES, analyzer = @Analyzer(definition = "autocompleteGenreNGramAnalyzer"))
	})
	@Column(name = "name", unique = true, nullable = false, length = 50)
	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@ContainedIn
	@ManyToMany(fetch = FetchType.LAZY)
	@JoinTable(name = "movie_genre", 
		joinColumns = { @JoinColumn(name = "genre_id", nullable = false, updatable = false) }, 
		inverseJoinColumns = { @JoinColumn(name = "movie_id", nullable = false, updatable = false) })
	public Set<Movie> getMovies() {
		return this.movies;
	}

	public void setMovies(Set<Movie> movies) {
		this.movies = movies;
	}
	
	@Override
	public int hashCode() {
		return HashCodeBuilder.reflectionHashCode(this, false);
	}

	@Override
	public boolean equals(Object obj) {
		return EqualsBuilder.reflectionEquals(this, obj, false);
	}
}
