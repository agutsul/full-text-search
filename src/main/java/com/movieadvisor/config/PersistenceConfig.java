package com.movieadvisor.config;

import java.io.File;
import java.util.Properties;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;

import org.apache.commons.dbcp2.BasicDataSource;
import org.hibernate.search.jpa.FullTextEntityManager;
import org.hibernate.search.jpa.Search;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.orm.hibernate4.HibernateExceptionTranslator;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@Configuration
@EnableTransactionManagement
@EnableJpaRepositories(basePackages = "com.movieadvisor.repository.dao")
@PropertySource("classpath:persistence.properties")
public class PersistenceConfig {

	@Autowired
	private Environment env;

	@Bean
	public DataSource dataSource() {
		final BasicDataSource ds = new BasicDataSource();
		
		ds.setDriverClassName(env.getProperty("jdbc.driverClassName"));
		ds.setUrl(env.getProperty("jdbc.url"));
		ds.setUsername(env.getProperty("jdbc.username"));
		ds.setPassword(env.getProperty("jdbc.password"));
		
		return ds;
	}

	@Bean
	public HibernateExceptionTranslator hibernateExceptionTranslator() {
		return new HibernateExceptionTranslator();
	}
	
	@Bean
	@Autowired
	public EntityManager entityManager(DataSource dataSource) {
		return entityManagerFactory(dataSource).createEntityManager();
	}
	
	@Bean
	@Autowired
	public EntityManagerFactory entityManagerFactory(DataSource dataSource) {
		HibernateJpaVendorAdapter vendorAdapter = new HibernateJpaVendorAdapter();
		
		vendorAdapter.setGenerateDdl(false);
		vendorAdapter.setShowSql(true);

		LocalContainerEntityManagerFactoryBean factoryBean = new LocalContainerEntityManagerFactoryBean();
		
		factoryBean.setJpaVendorAdapter(vendorAdapter);
		factoryBean.setPackagesToScan("com.movieadvisor.repository.domain");
		factoryBean.setDataSource(dataSource);

		factoryBean.setJpaProperties(hibernateProperties());

		factoryBean.afterPropertiesSet();
		
		return factoryBean.getObject();
	}
	
	@Bean
	@Autowired
	public JpaTransactionManager transactionManager(EntityManagerFactory entityManagerFactory) {
		JpaTransactionManager txManager = new JpaTransactionManager();

		txManager.setEntityManagerFactory(entityManagerFactory);
		txManager.setRollbackOnCommitFailure(true);

		return txManager;
	}
	
	@Bean
	@Autowired
	public FullTextEntityManager fullTextEntityManager(EntityManager entityManager) throws InterruptedException {
		FullTextEntityManager fullTextEntityManager = Search.getFullTextEntityManager(entityManager);
		
		File luceneIndexDir = new File(env.getProperty("lucene.index.dir"));
		if (luceneIndexDir.listFiles().length == 0) {
			fullTextEntityManager.createIndexer().startAndWait();
		}

		return fullTextEntityManager;
	}
	
	private Properties hibernateProperties() {
		final Properties properties = new Properties();		 
		
		properties.put(org.hibernate.cfg.Environment.DIALECT, env.getProperty("hibernate.dialect"));
		properties.put(org.hibernate.cfg.Environment.SHOW_SQL, true);
		properties.put(org.hibernate.cfg.Environment.FORMAT_SQL, false);
		properties.put(org.hibernate.cfg.Environment.DEFAULT_SCHEMA, "public");		
		
		properties.put("hibernate.search.default.directory_provider", "org.hibernate.search.store.impl.FSDirectoryProvider");
		properties.put("hibernate.search.default.indexBase", env.getProperty("lucene.index.dir")); 
		
		return properties;
	}

}
