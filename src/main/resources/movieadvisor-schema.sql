begin;

CREATE DATABASE movieadvice3
  WITH ENCODING='UTF8'
       OWNER=postgres
       CONNECTION LIMIT=-1;

CREATE TABLE actor
(
  id bigserial NOT NULL,
  first_name character varying(20) NOT NULL,
  last_name character varying(25) NOT NULL,
  CONSTRAINT actor_pkey PRIMARY KEY (id),
  CONSTRAINT actor_name_uc UNIQUE (first_name,last_name)
);

CREATE TABLE movie
(
	id bigserial NOT NULL,
	title character varying(100) NOT NULL,
	director_name character varying(50) NOT NULL,
	rating integer DEFAULT 0,
	CONSTRAINT movie_pkey PRIMARY KEY (id),
	CONSTRAINT movie_uc UNIQUE (title,director_name)
);

CREATE TABLE genre
(
  id bigserial NOT NULL,
  name character varying(520) NOT NULL,
  CONSTRAINT genre_pkey PRIMARY KEY (id),
  CONSTRAINT genre_name_uc UNIQUE (name)
);

CREATE TABLE movie_genre (
  movie_id  bigint REFERENCES movie(id)
                    ON UPDATE CASCADE
                    ON DELETE CASCADE,
  genre_id    bigint REFERENCES genre(id)
                    ON UPDATE CASCADE
                    ON DELETE CASCADE,
  PRIMARY KEY (movie_id, genre_id)
);

CREATE TABLE movie_actor (
  movie_id  bigint REFERENCES movie(id)
                    ON UPDATE CASCADE
                    ON DELETE CASCADE,
  actor_id    bigint REFERENCES actor(id)
                    ON UPDATE CASCADE
                    ON DELETE CASCADE,
  PRIMARY KEY (movie_id, actor_id)
);

commit;