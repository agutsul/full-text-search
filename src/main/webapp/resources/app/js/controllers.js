angular.module('movieAdvicer.controllers', [])

.controller('SearchCtrl', function($http, $scope, $templateCache, SearchService) {
	  
	$scope.searchResults = [];  	
	$scope.selectedSuggestion = '';
	
	$scope.suggestions = [];
	  
	$scope.getSuggestions = function(viewValue) {
		if (viewValue !== '' && !angular.isUndefined(viewValue)) {
			return SearchService.suggest(viewValue).then(function(suggestions){
				console.log(suggestions);
				$scope.suggestions = suggestions;
			}, function(error) {
				console.log(error);
				$scope.suggestions = [];
			});
		}
	  };
  
	  $scope.search = function() {
		  if ($scope.selectedSuggestion != '') {
			  SearchService.search($scope.selectedSuggestion).then(function(result) {
				  console.log(result);
				  $scope.searchResults = result;
			  }, function(error) {
				  console.log(error);
				  $scope.searchResults = [];
			  });
		  }
	  }
})
