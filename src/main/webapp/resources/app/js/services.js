angular.module('movieAdvicer.services', [])

.factory('SearchService', function($http, $q) {

	var suggestURL = "http://localhost:8080/movieadvisor2/movie/suggest";
	
	var searchURL = "http://localhost:8080/movieadvisor2/movie/search";

	var SearchService = {};
	
	SearchService.suggest = function(term) {		
		var deferred = $q.defer();
		
		$http.get(suggestURL + "?term=" + term)
		.success(function(data) {
			deferred.resolve(data);
		}).error(function(data) {
			console.log('error ' + data);
			deferred.reject(data);
		});
		
		return deferred.promise;
	};
	
	SearchService.search = function(term) {
		var deferred = $q.defer();
		
		$http.get(searchURL + "?term=" + term)
		.success(function(data) {
			deferred.resolve(data);
		}).error(function(data) {
			console.log('error ' + data);
			deferred.reject(data);
		});
		
		return deferred.promise;		
	};
	
	return SearchService;
});