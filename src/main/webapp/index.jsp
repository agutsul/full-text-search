<!DOCTYPE html>
<html lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<meta name="author" content="">
<link rel="icon" href="http://getbootstrap.com/favicon.ico">

<title>Movie Advisor</title>

<link href="${pageContext.request.contextPath}/resources/css/bootstrap-3.2.0/css/bootstrap.min.css" rel="stylesheet">
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/jquery-2.1.1/jquery-2.1.1.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/angular-1.3.0/angular.min.js"></script>
<!--script type="text/javascript" src="${pageContext.request.contextPath}/resources/css/bootstrap-3.2.0/js/bootstrap-3-typeahead.js"></script-->

<script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/angular-1.3.0/angular-strap/angular-animate.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/angular-1.3.0/angular-strap/angular-strap.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/angular-1.3.0/angular-strap/angular-strap.tpl.min.js"></script>

<script type="text/javascript" src="${pageContext.request.contextPath}/resources/app/js/app.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/app/js/services.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/app/js/controllers.js"></script>


<link href="http://getbootstrap.com/examples/jumbotron-narrow/jumbotron-narrow.css" rel="stylesheet">

<script src="http://getbootstrap.com/assets/js/ie-emulation-modes-warning.js"></script>
<script src="http://getbootstrap.com/assets/js/ie10-viewport-bug-workaround.js"></script>

<!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    
    <style type="text/css">
    	ul.typeahead, ul.dropdown-menu {
    		width: 95%;
    		padding-top: 5px;
    		padding-bottom: 5px;
    	}
		.footer {
			position: absolute;
			bottom: 0;
			height: 60px;
			background-color: #f5f5f5;
		} 
		.marketing {
			margin: 10px 0; 
			height: 560px; 
			overflow: auto;
		}
		.label-data {
			font-size: 100%; 
			margin-left:3px;
			margin-right:3px;
		}
		.dl-horizontal dt { 
			text-align: left; 
			width: 60px;
		}
		.dl-horizontal dd {
			margin-left: 70px;
		}
    </style>
</head>

<body ng-app="movieAdvicer">

	<div class="container"  ng-controller="SearchCtrl">
		<div class="header" style="margin-bottom: 10px;">
			<h3 class="text-muted"><span class="glyphicon glyphicon-film"></span>&nbsp;Movie Advisor</h3>						
		</div>

		<div class="row">
		  <div class="col-lg-9" >
		  				
			<input type="text" class="form-control ng-pristine ng-valid" 
				ng-model="selectedSuggestion" data-animation="animation-flipX" 
				ng-options="suggestion.score as suggestion.text for suggestion in getSuggestions($viewValue)" 
				placeholder="" bs-typeahead="" limit="10">		    		

		  </div><!-- /.col-lg-9 -->
		  <div class="col-lg-3">
		  		<button type="button" class="btn btn-primary" ng-click="search()" style="width: 100%;">
		  			<span class="glyphicon glyphicon-search"></span>&nbsp;
		  			<span><b>Search</b></span>
		  		</button>
		  </div><!-- /.col-lg-3 -->
		</div>			
		<!-- http://www.sitepoint.com/building-recipe-search-site-angular-elasticsearch/ -->
		<div class="row marketing">
			<div ng-model="searchResults" class="col-lg-12">
				<!--div ng-hide="searchResults.length">No results</div-->
				<div ng-repeat="movie in searchResults">

					<div class="panel panel-default" style="margin-bottom: 5px">
						<div class="panel-heading">
							<h3 class="panel-title" ng-click="showDetails = ! showDetails">
								<span class="glyphicon glyphicon-chevron-right" ng-class="{ 'hidden':  showDetails }"></span>
								<span class="glyphicon glyphicon-chevron-down" ng-class="{ 'hidden': ! showDetails }"></span>
								<b>{{movie.title}}</b>
								<span class="pull-right">Rating - <span class="badge">{{movie.rating}}</span></span>
							</h3>
						</div>
						<div class="panel-body" ng-show="showDetails">
							<dl class="dl-horizontal">
							  <dt>Genres:</dt>
							  <dd><span class="label label-info label-data" ng-repeat="genre in movie.genres">{{ genre.name }}</span></dd>
							</dl>
							<h5><b>Actors:</b></h5>
							<table class="table table-condensed">								
								<tr>
									<th>#</th>
									<th>First Name</th>
									<th>Last Name</th>
								</tr>
								<tr ng-repeat="actor in movie.actors">
									<td>{{$index + 1}}</td>
									<td>{{actor.firstName}}</td>
									<td>{{actor.lastName}}</td>
								</tr>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>

		<div class="footer">
			<div class="container">
				<p>NBD � Company 2014</p>
			</div>			
		</div>

	</div>
	<!-- /container -->


	<!-- Bootstrap core JavaScript
    ================================================== -->
	<!-- Placed at the end of the document so the pages load faster -->


</body>
</html>